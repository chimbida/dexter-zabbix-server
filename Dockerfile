FROM ubuntu:14.04

MAINTAINER Seu Nome <seu_e-mail@provedor>

ENV DEBIAN_FRONTEND noninteractive

COPY sources.list /etc/apt/sources.list
COPY zabbix-release_3.2-1+trusty_all.deb /tmp/zabbix-release_3.2-1+trusty_all.deb

RUN dpkg -i /tmp/zabbix-release_3.2-1+trusty_all.deb
RUN apt-get update

RUN apt-get install aptitude -y
RUN aptitude -y install zabbix-server-pgsql -R
RUN apt-get install zabbix-frontend-php zabbix-agent libapache2-mod-php5 php5 php5-curl php5-gd php5-intl php5-xmlrpc php5-pgsql php-pear php5-mcrypt php5-ldap php-net-ldap2 php5-imagick php-fpdf supervisor rsyslog -y
RUN apt-get clean autoclean && apt-get autoremove -y
RUN rm -rf /var/lib/{apt,dpkg,cache,log}/

COPY rsyslog.conf /etc/rsyslog.conf
COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf
RUN mkdir -p /var/log/supervisor
RUN touch /var/log/zabbix/zabbix_server.log

COPY apache.conf /etc/zabbix/apache.conf
COPY zabbix.conf.php /etc/zabbix/web/zabbix.conf.php
COPY zabbix_server.conf /etc/zabbix/zabbix_server.conf
COPY zabbix_agentd.conf /etc/zabbix/zabbix_agentd.conf

RUN chown www-data. /etc/zabbix/web/zabbix.conf.php
RUN chown zabbix. /var/log/zabbix/zabbix_server.log

ENV TERM xterm
EXPOSE 80 10050 10051 

CMD ["/usr/bin/supervisord"]
